This is a set of Jupyter Notebooks used for plotting netcdf output from CanESM5-PAM, GEM-MACH-PAM, and the PAM single column model.

These notebooks were made for quickly assessing model output, but are not intended for general use.
No guarantee is given that they are functional, legible, or free of errors.
